import requests
from functools import wraps
import logging
import xmltodict
from bs4 import BeautifulSoup

log = logging.getLogger('serverlogger')

from smsserv3372.message import *

class API:

    def __init__(self):
        self.home_url = 'http://{}/'
        self.sms_url = 'html/smsinbox.html'
        self.monitoring_status = 'api/monitoring/status/'
        self.session_api = 'webserver/SesTokInfo/'
        self.sms_send_url = 'api/sms/send-sms'
        self.sms_receive_url = 'api/sms/sms-list'
        self.sms_count_url = 'api/sms/sms-count'
        self.sms_delete_url = 'api/sms/delete-sms'
        self.sms_set_read_url = 'api/sms/set-read'
        self.get_sms_xml = '<?xml version="1.0" encoding="UTF-8"?><request><PageIndex>1</PageIndex><ReadCount>{}' \
                           '</ReadCount><BoxType>1</BoxType><SortType>0</SortType>' \
                           '<Ascending>0</Ascending><UnreadPreferred>1</UnreadPreferred></request>'
        self.sms_id_xml = '<?xml version="1.0" encoding="UTF-8"?><request><Index>{}</Index></request>'
        self.sms_send_xml = "<?xml version='1.0' encoding='UTF-8'?><request><Index>-1</Index><Phones><Phone>{}" \
                            "</Phone></Phones><Sca></Sca><Content>{}</Content><Length>{}</Length>" \
                            "Reserved>1</Reserved><Date>-1</Date></request>"


class Dongle:

    def __init__(self, host):
        self.api = API()
        self.host = host
        self.session = requests.Session()


    def check_dongle(self):
        @wraps(self)
        def wrapper(self, *args, **kwargs):
            try:
                if self.session.get(self.home_url).status_code == 200:
                    self.connected = True
                    return self(self, *args, **kwargs)
            except requests.exceptions.Timeout:
                log.debug('Connection timed out')
                self.connected = False
                return None
            except requests.exceptions.ConnectionError:
                log.debug('Connection error')
                self.connected = False
                return None

        return wrapper

    def check_network(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            r = xmltodict.parse(self.session.get(self.home_url + self.monitoring_status).content)
            if int(r['response']['ConnectionStatus']) == 901:
                log.info('network online')
                return func(self, *args, **kwargs)
            log.debug('No signal.')
            return None

        return wrapper

    def refresh_sms_token(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            self.get_sms_token()
            return func(self, *args, **kwargs)

        return wrapper

    @check_dongle
    def get_sms_token(self):
        html = BeautifulSoup(self.session.get(self.home_url + self.sms_url).content, 'html.parser')
        self.sms_token = html.find('meta').get('content')
        self.headers = {'__RequestVerificationToken': self.sms_token,
                        "X-Requested-With": "XMLHttpRequest",
                        "Content-Type": 'text/xml'}
    @refresh_sms_token
    def _make_api_get_request(self, url):
        return xmltodict.parse(self.session.get(url=url,
                                                headers=self.headers).text)['response']

    @refresh_sms_token
    def _make_api_post_request(self, url, data):
        return xmltodict.parse(
            self.session.post(url=url,
                              data=data.encode('utf-8'),
                              headers=self.headers).content.decode('utf-8'))['response']

    def get_sms_count(self):
        return self._make_api_get_request(self.api.home_url + self.api.sms_count_url, data='')

    def get_sms_list(self, local_inbox):
        data = self.api.get_sms_xml.format(str(local_inbox))
        messages = self._make_api_post_request(url=self.api.home_url + self.api.sms_receive_url,
                                              data=data)['Messages']['Message']
        if local_inbox == 1:
            messages = [messages]
        sms_list = [SMS(**message) for message in messages]
        return sms_list

    def mark_read_on_dongle(self, sms_list):
        result = {}
        for sms in sms_list:
            if sms.smstat == '0':
                data = self.api.sms_id_xml.format(sms.index)
                result[sms.index] = self._make_api_post_request(url=(self.api.home_url + self.api.sms_set_read_url),
                                                               data=data)
        if len(result) > 0:
            log.info('marked read on dongle : {}'.format(str(result)))

    def delete_from_incoming(self, sms_list):
        result = {}
        for sms in sms_list:
            data = self.api.sms_id_xml.format(sms.index)
            result[sms.index] = self._make_api_post_request(url=(self.api.home_url + self.api.sms_delete_url),
                                                           data=data)
        return result

    def send_sms(self, sms_list):
        result = []
        for sms in sms_list:
            data = self.api.sms_send_xml.format(sms.phone, sms.content, sms.len)
            if self.make_api_post_request(url=self.api.home_url + self.api.sms_send_url,
                                          data=data) == 'OK':
                result.append(sms)
                log.info('Sent sms {} - {}'.format(str(sms.index), sms.content))
        return result