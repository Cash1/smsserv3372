from sqlalchemy.orm import *
from sqlalchemy import create_engine
import logging
import sys
from sqlalchemy.sql import select

from smsserv3372.api import *
import logging_setup
from smsserv3372.message import OutgoingSMS

log = logging.getLogger('serverlogger')


class Server:

    def __init__(self):
        self.sms_received = []
        self.sms_to_send = []

    def check_new_sms(self):
        sms_count = dongle.get_sms_count()
        local_unread = int(sms_count['LocalUnread'])
        local_inbox = int(sms_count['LocalInbox'])
        if local_unread > 0:
            log.info('Received {} new sms'.format(local_unread))
            self.sms_received = dongle.get_sms_list(local_inbox)
            _ = [log.info('{} - {} - {}'.format(sms.index, sms.phone, sms.content)) for sms in self.sms_received]

    def save_new_sms_in_db(self):
        session.Session.add_all(self.sms_received)
        session.Session.commit()

    def check_sms_to_send(self):
        sms_to_send = session.query(OutgoingSMS).options(subquery(OutgoingSMS.phone)).all()
        self.sms_to_send = [message._dict() for message in sms_to_send]

    def record_sent_sms(self):
        sent_list = [SentSMS(sms.phone, sms.content, sms.date) for sms in self.sent_to_send]
        session.add_all(sent_list)
        for sms in self.sms_to_send:
            session.Session.delete(sms)
        session.Session.commit()

    def run(self):
        while True:
            self.check_new_sms()
            self.mark_read_on_dongle(self.sms_received)
            if len(self.sms_received) > 0:
                self.save_new_sms_in_db()
                dongle.delete_from_incoming(self.sms_received)
                self.sms_received = []
            self.check_sms_to_send()
            if len(self.sms_to_send) > 0:
                dongle.send_sms(self.sms_to_send)
                self.record_sent_sms()


if __name__ == '__main__':
    try:
        engine = create_engine('postgresql+psycopg2://postgres:postgres@192.168.10.25:5432/smsserver')
        Session = session(bind=engine)
        session = Session()
        dongle = Dongle('192.168.8.1')
        server = Server(donglehost='192.168.8.1')
        server.run()
    except KeyboardInterrupt:
        session.close()
        sys.exit()
