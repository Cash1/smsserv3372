import datetime
from sqlalchemy import *
from sqlalchemy.orm import mapper
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class SMS:

    def __init__(self, message='', phone='', **kwargs):
        self.content = message
        self.phone = phone
        self.date = datetime.datetime.now()
        self.len = len(message)
        self.index = 0
        self.savetype = 4
        self.priority = 0
        self.smstype = 1
        for kwarg in kwargs:
            setattr(self, kwarg.lower(), kwargs[kwarg])

    def _dict(self):
        return {attr: getattr(self, attr) for attr in dir(self) if not attr.startswith('_')}


class SentSMS(Base, SMS):
    __tablename__ = 'sentmessages'
    index = Column(Integer, primary_key=True)
    phone = Column(String)
    content = Column(String)
    date = Column(TIMESTAMP)

    def __init__(self, phone, content, date):
        Base.__init__(self)
        SMS.__init__(self)
        self.phone = phone
        self.content = content
        self.date = date


class IncomingSMS(Base, SentSMS):
    __tablename__ = 'incomingsms'
    index = Column(Integer, primary_key=True)
    phone = Column(String)
    content = Column(String)
    date = Column(TIMESTAMP)
    sca = Column(String)
    savetype = Column(String)
    priority = Column(Integer)
    smstype = Column(Integer)
    status = Column(Integer)

    def __init__(self, phone, content, date, sca, savetype, priority, smstype, status):
        Base.__init__(self)
        SentSMS.__init__(self, phone, content, date)
        self.sca = sca
        self.savetype = savetype
        self.priority = priority
        self.smstype = smstype
        self.status = 1


class OutgoingSMS(Base, SentSMS):
    __tablename__ = 'sms_to_send'
    index = Column(Integer, primary_key=True)
    phone = Column(String, ForeignKey('contact.id'))
    content = Column(String)

    def __init__(self, phone, content, date):
        Base.__init__(self)
        SentSMS.__init__(self, phone, content, date)

