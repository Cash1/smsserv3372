import requests
import xmltodict
import datetime
from functools import wraps
import logging
from bs4 import BeautifulSoup
import loggin_setup
import psycopg2
import psycopg2.extras as extras

log = logging.getLogger('serverlogger')


class SMS:

    def __init__(self, message='', phone='', **kwargs):
        self.content = message
        self.phone = phone
        self.date = datetime.datetime.now()
        self.len = len(message)
        self.index = 0
        self.savetype = 4
        self.priority = 0
        self.smstype = 1
        for kwarg in kwargs:
            setattr(self, kwarg.lower(), kwargs[kwarg])

    def save_sql(self):
        sql =  'INSERT INTO incomingsms (id, phone, content, date, sca, savetype, priority, smstype, status)' \
              ' values ({}, \'{}\', \'{}\', \'{}\', \'{}\', {}, {}, {}, True);'.format(
            self.index, self.phone, self.content, self.date, self.sca, self.savetype, self.priority, self.smstype)

        return sql

    def del_sql(self):
        sql = 'UPDATE incomingsms set status = False where id = {};'.format(self.index)
        return sql

    def save_sent_sql(self):
        sql = 'INSERT INTO sentmessages (id, phone, content, date)' \
              ' values ({}, \'{}\', \'{}\', \'{}\');'.format(
            self.index, self.phone, self.content, self.date)
        return sql


class Server:

    def __init__(self, host):
        self.home_url = 'http://{}/'.format(host)
        self.api = self.home_url + 'api/'
        self.sms_url = 'html/smsinbox.html'
        self.monitoring_status = 'monitoring/status/'
        self.session_api = 'webserver/SesTokInfo/'
        self.connected = False
        self.sms_send_url = 'api/sms/send-sms'
        self.sms_receive_url = 'api/sms/sms-list'
        self.sms_count_url = 'api/sms/sms-count'
        self.sms_delete_url = 'api/sms/delete-sms'
        self.session = requests.Session()
        self.db_connection = psycopg2.connect("dbname='smsserver' host='192.168.10.25' user='postgres' password='postgres'")
        self.cursor = self.db_connection.cursor()
        self.cursor2 = self.db_connection.cursor(cursor_factory=extras.DictCursor)

    def check_dongle(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            try:
                if self.session.get(self.home_url).status_code == 200:
                    self.connected = True
                    return func(self, *args, **kwargs)
            except requests.exceptions.Timeout:
                log.debug('Connection timed out')
                self.connected = False
                return None
            except requests.exceptions.ConnectionError:
                log.debug('Connection error')
                self.connected = False
                return None
        return wrapper

    def check_network(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            r = self.session.get(self.api+self.monitoring_status).content
            d = xmltodict.parse(r)
            if int(d['response']['ConnectionStatus']) == 901:
                print('network online')
                return func(self, *args, **kwargs)
            log.debug('No signal.')
            return None
        return wrapper

    def refresh_session(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            self.get_sms_token()
            return func(self, *args, **kwargs)
        return wrapper

    @check_dongle
    def get_sms_token(self):
        html = BeautifulSoup(self.session.get(self.home_url + self.sms_url).content, 'html.parser')
        self.sms_token = html.find('meta').get('content')
        self.headers = {'__RequestVerificationToken': self.sms_token,
                   "X-Requested-With": "XMLHttpRequest",
                   "Content-Type": 'text/xml'}

    @check_dongle
    def connect(self):
        try:
            self.get_sms_token()
            r = xmltodict.parse(self.session.get(self.api + self.session_api).content)
            self.session_id = r['response']['SesInfo'][10:]
            self.session_token = r['response']['TokInfo']
            print('connected to dongle')
            return True
        except:
            log.debug('Failed to get session ID.')
            return False

    @check_dongle
    @check_network
    def receiveSMS(self):
        urlc = self.home_url + self.sms_count_url
        dict = xmltodict.parse(self.session.get(urlc, headers=self.headers).content.decode('utf-8'))
        countmessages = int(dict['response']['LocalInbox'])
        if int(countmessages) > 0:
            print(countmessages)
            url = self.home_url + self.sms_receive_url
            data = '<?xml version="1.0" encoding="UTF-8"?><request><PageIndex>1</PageIndex><ReadCount>{}</ReadCount><BoxType>1</BoxType><SortType>0</SortType>' \
                   '<Ascending>0</Ascending><UnreadPreferred>1</UnreadPreferred></request>'.format(countmessages)
            log.info(self.session.post(url,headers=self.headers, data=data).content.decode('utf-8'))
            messages = xmltodict.parse(self.session.post(url,headers=self.headers, data=data).content.decode('utf-8'))['response']['Messages']['Message']
            print(messages)
            sms_list = self._saveSMStodb(messages)
            self._deleteSMSfromdongle(sms_list)

    def _saveSMStodb(self, messages):
        sms_list = []
        for message in messages:
            sms = SMS(**message)
            print('saving to db {}'.format(sms.content))
            self.cursor.execute(sms.save_sql())

            self.db_connection.commit()
            sms_list.append(sms)
            print('here')
        return sms_list

    def _saveSentSMStodb(self, messages):
        sms_list = []
        for message in messages:
            sms = SMS(**message)
            print('saving to db {}'.format(sms.content))
            self.cursor.execute(sms.save_sent_sql())
            print('here')
            self.db_connection.commit()

            sms_list.append(sms)
            print(sms_list)
        return sms_list


    def _deleteSMSfromdongle(self, sms_list):
        for sms in sms_list:
            data = '<?xml version="1.0" encoding="UTF-8"?><request><Index>{}</Index></request>'.format(sms.index)
            print('deleting from dongle {}'.format(sms.content))
            print(data)
            print(self._delSingleSMSFromDongle(data))
        return True

    @refresh_session
    def _delSingleSMSFromDongle(self, data):
        return (self.session.post(url=self.home_url + self.sms_delete_url, data=data, headers=self.headers).text)

    def _deleteSMSfromdb(self, sms):
        self.cursor.execute(sms.del_sql())
        self.db_connection.commit()

    def deleteSMS(self, sms):
        self._deleteSMSfromdongle(sms)
        self.deleteSMSfromdb(sms)

    @refresh_session
    @check_dongle
    @check_network
    def sendSMS(self, message):
        data = "<?xml version='1.0' encoding='UTF-8'?><request><Index>-1</Index><Phones><Phone>{}</Phone></Phones>" \
               "<Sca></Sca><Content>{}</Content><Length>{}</Length><Reserved>1</Reserved><Date>-1</Date>" \
               "</request>".format(message.phone, message.content, message.len).encode('utf-8')
        print(data)
        print(self.headers)
        r = self.session.post(url=self.home_url + self.sms_send_url, data=data, headers=self.headers).text
        print((r)) # <response>OK</response>


    def send_template(self, template_id, contacts=None):
        self.cursor.execute('SELECT content from templates where id = {}'.format(template_id))
        template = self.cursor.fetchone()[0]
        print(template)
        if contacts is None:

            #print(self.cursor2)
            self.cursor2.execute('SELECT * from contacts;')
            recepients = self.cursor2.fetchall()
            print(recepients)
            sent_list = []
            for recepient in recepients:
                message = template.format(recepient['name'])
                sms = SMS(message=message, phone=recepient['phone'])
                self.sendSMS(sms)
                self.connect()
                sent_list.append(sms)
            print(sent_list)
            self._saveSentSMStodb(sent_list)

if __name__ == '__main__':
    try:
        server = Server('192.168.8.1')
        server.connect()
        print('here')
        #server.send_template(1, 1)
        server.receiveSMS()
    except KeyboardInterrupt:
        server.db_connection.close()
    except Exception as e:
        print(e)
        server.db_connection.close()
    finally:
        server.db_connection.close()

# delete:
# post
# <request><Index>40001</Index></request>
# read:
# post
# <request><Index>40001</Index></request>