import os
import logging
import logging.handlers as handlers

ENAGBLE_LOGGING = True
LOG_PATH = 'logs'

formatter = logging.Formatter("%(asctime)s   -%(module)s - %(message)s")

if ENAGBLE_LOGGING:
    server_logger = logging.getLogger('serverlogger')
    server_handler = handlers.TimedRotatingFileHandler(os.path.join(LOG_PATH, 'server.log'), 'D', 1, 100, encoding='utf-8')
    server_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler()
    server_logger.addHandler(server_handler)
    server_logger.addHandler(stream_handler)
    server_logger.setLevel(logging.DEBUG)
