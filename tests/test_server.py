import pytest
import requests

from server import Server


@pytest.yield_fixture
def my_session():
    original_session = requests.Session
    requests.Session = MockSession()
    yield
    requests.Session = original_session


class MockResponse:
    def __init__(self, type):
        if type == 'get':
            self.content = 'get content'
        if type == 'post':
            self.text = 'post text'

    @staticmethod
    def content(self, content):
        return (content)

class MockSession:

    def __init__(self):
        pass

    def post(self, url, data, headers):
        pass

        @staticmethod
        def text(self):
            return 'post text'

    def get(self, url):
        return MockResponse('get')


def test_class(my_session):
    s = requests.Session
    assert s.get('url').content == 'get content'